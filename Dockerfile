FROM maven:3-jdk-8

ADD ./ .

EXPOSE 8888

CMD mvn clean install && java -jar target/config-server.jar